from django.shortcuts import render, get_object_or_404

# Create your views here.
from .models import Post, Category, Author
from .forms import MessageForm

def portfolio(request):
	context = {

	}
	return render(request, 'portfolio.html', context)

def blog_home(request):
	posts = Post.objects.all().order_by('-id')
	category = Category.objects.all().order_by('id')
	author = Author.objects.all().order_by('id')
	context = {
		'posts' : posts,
		'category': category,
		'author' : author,
	}
	return render(request, 'blog_home.html', context)


def contact(request):
	form = MessageForm(request.GET or None)
	if form.is_valid():
		name = form.cleaned_data['name']
		email = form.cleaned_data['email']
		message = form.cleaned_data['message']
		instance = form.save(commit=False)
		instance.save()
	context = {
		"form": form,
    }
	return render(request, 'portfolio.html', context)

def particular_post(request, slug):
	category = Category.objects.all().order_by('id')
	author = Author.objects.all().order_by('id')
# 	# posts = Post.objects.all().order_by('id')
	posts = get_object_or_404(Post, slug=slug)
	context = {
# 		'name': "Ashraful",
		'title': posts.title,
		'posts': posts,
		'category': category,
		'author' : author,
	}
	return render(request, 'particular_post.html', context)