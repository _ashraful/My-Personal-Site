from django.contrib import admin

# Register your models here.
from .models import Author, Category, Post

class PostAdmin(admin.ModelAdmin):
	prepopulated_fields = {'slug' : ('title',)}
	list_display = ['title', 'author', 'category', 'post_date']
	search_fields = ['title', 'author', 'category']
	list_filter = ['category']
	class Meta:
		model = Post

admin.site.register(Post, PostAdmin)

class AuthorAdmin(admin.ModelAdmin):
	list_display = ['name', 'email']
	search_fields = ['name']
	list_filter = ['name']
	class Meta:
		model = Author

admin.site.register(Author, AuthorAdmin)

class CategoryAdmin(admin.ModelAdmin):
	prepopulated_fields = {'slug' : ('name',)}
	list_display = ['name']
	search_fields = ['name']
	list_filter = ['name']
	class Meta:
		model = Post

admin.site.register(Category, CategoryAdmin)