# An url configuration only for blog application

from django.conf.urls import url
from django.contrib import admin

# from blog.views import home

urlpatterns = [
	url(r'^$', "blog.views.blog_home"),
	url(r'^(?P<slug>.+)/$', "blog.views.particular_post"),
    
]
