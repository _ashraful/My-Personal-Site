from django.db import models

from ckeditor.fields import RichTextField

# Create your models here.
class Category(models.Model):
	name = models.CharField(max_length=20)
	slug = models.SlugField(max_length=25, unique=True)

	def __str__(self):
		return self.name

class Author(models.Model):
	name = models.CharField(max_length=20)
	email = models.EmailField()

	def __str__(self):
		return self.name

class Post(models.Model):
	title = models.CharField(max_length=200)
	slug = models.SlugField(max_length=200, unique=True)
	category = models.ForeignKey(Category)
	author = models.ForeignKey(Author)
	body = RichTextField()
	post_date = models.DateTimeField(auto_now_add=True, auto_now=False)

	def __str__(self):
		return self.title

# Get message from blog reader...
class Message(models.Model):
	name = models.CharField(max_length=30)
	email = models.EmailField()
	message = models.TextField()
	date_of_mgs = models.DateTimeField(auto_now_add=True, auto_now=False)

	def __str__(self):
		return self.name