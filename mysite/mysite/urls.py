
from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^$', "blog.views.portfolio"),
	url(r'^blog/', include("blog.urls")),
    url(r'^admin/', admin.site.urls),

    # (r'^ckeditor/', include('ckeditor.urls')),
]
